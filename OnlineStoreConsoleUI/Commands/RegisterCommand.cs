﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using OnlineStoreConsoleUI.Controllers;
using OnlineStoreConsoleUI.Models;
using OnlineStoreDomain.Repositories;

namespace OnlineStoreConsoleUI.Commands
{
    public class RegisterCommand : CommandBase
    {
        public override string Name => "register";
        public override string Description => "Register a new customer";

        public override Controller Execute<T>(T controller)
        {
            var customer = new Customer();
            
            CreateLogin(customer);
            CreatePassword(customer);
            CreateName(customer);
            CreateBirthDate(customer);
            CreateEmail(customer);
            CreatePhoneNumber(customer);
            CreateAddress(customer);

            Console.WriteLine("You successfully registered");
            
            return new CustomerController(customer);
        }

        private static void CreateAddress(Customer customer)
        {
            while (true)
            {
                Console.WriteLine("Enter your address");
                var address = Console.ReadLine();

                if (address is { })
                {
                    customer.Address = address;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }

        private static void CreatePhoneNumber(Customer customer)
        {
            while (true)
            {
                Console.WriteLine("Enter a phone number");
                var number = Console.ReadLine();

                var pattern = @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}";
                if (number is { } && Regex.IsMatch(number, pattern))
                {
                    customer.PhoneNumber = number;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect phone number");
                Console.ResetColor();
            }
        }

        private static void CreateEmail(Customer customer)
        {
            while (true)
            {
                Console.WriteLine("Enter an email");
                var email = Console.ReadLine();

                try
                {
                    if (email is { })
                        _ = new MailAddress(email);
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Incorrect date of birth");
                    Console.ResetColor();
                }

                customer.Email = email;

                break;
            }
        }

        private static void CreateBirthDate(Customer customer)
        {
            DateTime birthDate;
            
            while (true)
            {
                Console.WriteLine("Enter a date of birth");

                if (DateTime.TryParse(Console.ReadLine(), out birthDate)
                    && DateTime.Now.Year - birthDate.Year < 105 && DateTime.Now.Year - birthDate.Year > 6)
                {
                    customer.BirthDate = birthDate;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect date of birth");
                Console.ResetColor();
            }
        }

        private static void CreateName(Customer customer)
        {
            while (true)
            {
                Console.WriteLine("Enter a first and second name");
                var name = Console.ReadLine();

                var pattern = @"[a-zA-Z]";
                if (name is { } && Regex.IsMatch(name, pattern))
                {
                    customer.Name = name;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect name");
                Console.ResetColor();
            }
        }

        private static void CreatePassword(Customer customer)
        {
            while (true)
            {
                Console.WriteLine("Enter a password");
                var password = Console.ReadLine();

                var pattern = @"^(.{0,7}|[^0-9]*|[^A-Z])$";
                if (!(password is { } && Regex.IsMatch(password, pattern)))
                {
                    continue;
                }

                Console.WriteLine("Enter the same password");
                var checkPass = Console.ReadLine();

                if (checkPass == password)
                {
                    customer.Password = password;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect password");
                Console.ResetColor();
            }
        }

        private static void CreateLogin(Customer customer)
        {
            while (true)
            {
                Console.WriteLine("Enter a login");
                var login = Console.ReadLine();

                if (CustomerRepository.Customers.Any(c => c.Login == login))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("The user with this login is already exists");
                    Console.ResetColor();

                    continue;
                }

                var pattern = "^[a-zA-Z0-9]+$";
                if (login is { } && Regex.IsMatch(login, pattern))
                {
                    customer.Login = login;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect login");
                Console.ResetColor();
            }
        }
    }
}