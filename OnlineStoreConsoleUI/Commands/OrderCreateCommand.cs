﻿using OnlineStoreConsoleUI.Controllers;

namespace OnlineStoreConsoleUI.Commands
{
    public class OrderCreateCommand : CommandBase
    {
        public override string Name => "checkout";
        public override string Description => "Create an order";

        public override Controller Execute<T>(T controller)
        {
            if (!(controller is CustomerController customerController))
                return controller;

            var currentOrder = customerController.CurrentCustomer.CurrentOrder;

            foreach (var toOrder in customerController.CurrentCustomer.ToOrder)
            {
                currentOrder.Value.AddProductOrder(toOrder);
            }
            
            customerController.CurrentCustomer.PlaceOrder(currentOrder.Value);
           
            return customerController;
        }
    }
}