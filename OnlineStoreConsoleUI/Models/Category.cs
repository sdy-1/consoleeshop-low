﻿namespace OnlineStoreConsoleUI.Models
{
    public enum Category
    {
        Fruit,
        Drink,
        Vegetable,
        Meat
    }
}