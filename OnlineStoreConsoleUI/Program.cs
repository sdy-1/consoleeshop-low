﻿using System;
using System.Linq;
using OnlineStoreConsoleUI.Commands;
using OnlineStoreConsoleUI.Controllers;

namespace OnlineStoreConsoleUI
{
    static class Program
    {
        private static void Main(string[] args)
        {
            Controller controller = new GuestController();
            new HelpCommand().Execute(controller);

            while (true)
            {
                Console.Write("> ");
                var line = Console.ReadLine();
                if (line == "exit")
                    break;
                var command = controller.Commands.FirstOrDefault(x => x.Name == line);

                if (command is null)
                {
                    Console.WriteLine("There is no such command, type help.");
                    continue;
                }

                controller = command.Execute(controller);
            }
        }
    }
}