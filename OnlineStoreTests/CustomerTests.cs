using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OnlineStoreConsoleUI.Models;
using OnlineStoreConsoleUI.Repositories;
using OnlineStoreDomain.Repositories;

namespace OnlineStoreTests
{
    public class CustomerTests
    {
        private List<Customer> _customers;

        [SetUp]
        public void Setup()
        {
            _customers = new List<Customer>
            {
                new Customer { Login = "customer1", Password = "pass1" },
                new Customer { Login = "customer2", Password = "pass2" },
                new Customer { Login = "customer3", Password = "pass3" }
            };
        }

        [Test]
        public void SignUpCustomer_ReturnsCollectionWithNewCustomer()
        {
            Customer customer = new Customer {Login = "customer4"};
            
            Register(customer);
            
            Assert.AreEqual(4, _customers.Count);
            Assert.AreEqual("customer4", _customers[3].Login);
        }

        [Test]
        public void AddToOrder_ReturnsCollectionWithNewOrder()
        {
            Product po = ProductRepository.Products.ElementAt(0);
            
            _customers[0].AddProductToOrder(po, 2);
            
            Assert.AreEqual(_customers[0].ToOrder.Count(), 1);
        }

        [Test]
        public void PlaceOrder_ReturnsCollectionWithNewOrder()
        {
            Order order = new Order();
            _customers[0].PlaceOrder(order);
            Assert.AreEqual(_customers[0].PlacedOrders.Count(), 1);
        }

        [Test]
        public void PlaceOrder_ReturnsEmptyCurrentOrder()
        {
            Order order = new Order();
            _customers[0].PlaceOrder(order);
            Assert.IsFalse(_customers[0].CurrentOrder.IsValueCreated);
        }

        [Test]
        public void CancelOrder_ReturnsCollectionWithoutOrder()
        {
            Order order = new Order();
            
            _customers[0].PlaceOrder(order);
            _customers[0].CancelOrder(order);
            
            Assert.IsEmpty(_customers[0].PlacedOrders);
        }

        [Test]
        public void ChangeStatus_ReturnsTheOrderWithReceivedStatus()
        {
            Order order = new Order {Status = OrderStatus.Canceled};
            
            _customers[0].PlaceOrder(order);
            _customers[0].ChangeStatus(order);
            
            Assert.AreEqual(order.Status, OrderStatus.Received);
        }
        
        private void Register(Customer customer)
        {
            _customers.Add(customer);
        }
    }
}